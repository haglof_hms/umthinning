#pragma once

#include "Resource.h"

class CUMThinningDB;

class CPlotsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPlotsFormView)

protected:
	CPlotsFormView();           // protected constructor used by dynamic creation
	virtual ~CPlotsFormView();

	CUMThinningDB *m_pDB;
	CMyReportCtrl m_report;

public:
	enum { IDD = IDD_FORMVIEW11 };

	void PopulateData();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();

	CXTResizeGroupBox m_group1;
	CMyExtEdit m_avlagg;
	CMyExtEdit m_drvenhet;
	CMyExtEdit m_datum;
	CMyExtEdit m_ytor;
	CMyExtEdit m_koordinat;

	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};
