#include "stdafx.h"
#include "defines.h"
#include "ResLangFileReader.h"
#include "SpecieSettingsFormView.h"
#include "ThinningTemplate.h"
#include "UMThinningGenerics.h"

IMPLEMENT_DYNCREATE(CSpecieSettingsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CSpecieSettingsFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)
END_MESSAGE_MAP()

CSpecieSettingsFormView::CSpecieSettingsFormView():
CXTResizeFormView(CSpecieSettingsFormView::IDD),
m_bInitialized(false),
m_pTemplate(NULL),
m_nSpcID(0)
{
}

CSpecieSettingsFormView::~CSpecieSettingsFormView()
{
}

void CSpecieSettingsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CSpecieSettingsFormView::CreateWnd(DWORD dwStyle, const RECT &rect, CWnd *pParent, UINT nId, CCreateContext *pCC)
{
	return Create(NULL, NULL, dwStyle, rect, pParent, nId, pCC);
}

void CSpecieSettingsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if( !m_bInitialized )
	{
		CXTPPropertyGridItem *pCategory;
		vecUCFunctions vecFunc;
		vecUCFunctionList vecFuncList;

		RLFReader *xml = new RLFReader;
		CString langFile = getLanguageFN(getLanguageDir(), _T("UMTemplates"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
		xml->Load(langFile);

		// Funktioner
		pCategory = m_propertyGrid.AddCategory( xml->str(201) );

		// Höjdfunktioner
		getHeightFunctions( vecFunc, vecFuncList );
		AddFunctions( xml->str(2010), vecFunc, pCategory, IDC_HOJD );
		m_sCalcAsHeight = xml->str(2011);

		// Volymfunktioner
		getVolumeFunctions( vecFunc, vecFuncList );
		AddFunctions( xml->str(2012), vecFunc, pCategory, IDC_VOLYM );
		m_sCalcAsVolume = xml->str(2013);

		// Barkfunktioner
		getBarkFunctions( vecFunc, vecFuncList );
		AddFunctions( xml->str(2014), vecFunc, pCategory, IDC_BARK );
		m_sCalcAsBark = xml->str(2015);

		// Volym över stubbe
		getVolumeFunctions_ub( vecFunc, vecFuncList );
		AddFunctions( xml->str(2016), vecFunc, pCategory, IDC_VOLYMUB );
		m_sCalcAsVolumeUB = xml->str(2017);


		// Övriga inställningar
		pCategory = m_propertyGrid.AddCategory( xml->str(203) );

		// H25
		CXTPPropertyGridItemDouble *pH25 = (CXTPPropertyGridItemDouble*)pCategory->AddChildItem(new CXTPPropertyGridItemDouble(xml->str(2036), 0.0, _T("%.1f")));
		pH25->SetID(IDC_H25);

		// Grönkroneandel
		CXTPPropertyGridItemDouble *pGcrown = (CXTPPropertyGridItemDouble*)pCategory->AddChildItem(new CXTPPropertyGridItemDouble(xml->str(2037), 0.0, _T("%.0f")));
		pGcrown->SetID(IDC_GCROWN);

		if(xml) delete xml;
	}

	// Expand all categories
	_DoCollapseExpand( m_propertyGrid.GetCategories(), &CXTPPropertyGridItem::Expand );

	m_bInitialized = true;
}

void CSpecieSettingsFormView::AddFunctions(LPCTSTR lpszName, vecUCFunctions &vecFunc, CXTPPropertyGridItem *pParent, int nID)
{
	CXTPPropertyGridItem *pItem = pParent->AddChildItem(new CXTPPropertyGridItem(lpszName));
	for( UINT i = 0; i < vecFunc.size(); i++ )
	{
		pItem->GetConstraints()->AddConstraint( vecFunc[i].getName(), vecFunc[i].getIdentifer() );
	}
	pItem->SetFlags( xtpGridItemHasComboButton );
	pItem->SetConstraintEdit( FALSE );
	pItem->SetID(nID);
}

void CSpecieSettingsFormView::SetupSubFunctions(CXTPPropertyGridItem *pItem, CString caption, vecUCFunctions &vecFunc, vecUCFunctionList &vecFuncList, int nID)
{
	CXTPPropertyGridItem *pChild;
	CString str;
	int idx;

	// Make sure child element exist
	if( !pItem->HasChilds() )
	{
		pChild = pItem->AddChildItem(new CXTPPropertyGridItem(caption));
	}
	else
	{
		pChild = pItem->GetChilds()->GetAt(0);
	}
	pItem->Expand();

	idx = pItem->GetConstraints()->GetCurrent();
	pChild->GetConstraints()->RemoveAll();
	pChild->SetValue(_T(""));
	pChild->SetID(nID);

	// List sub functions for this main function
	for( UINT i = 0; i < vecFuncList.size(); i++ )
	{
		// Check if subfunction belongs to the active item
		if( vecFuncList[i].getID() == vecFunc[idx].getIdentifer() )
		{
			str = CString(vecFuncList[i].getSpcName()) + CString(", ") + CString(vecFuncList[i].getFuncArea());
			pChild->GetConstraints()->AddConstraint( str, i );
			pChild->SetFlags( xtpGridItemHasComboButton );
			pChild->SetConstraintEdit( FALSE );
		}
	}
}

int CSpecieSettingsFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CXTResizeFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	m_propertyGrid.Create(CRect(0, 0, 0, 0), this, IDC_PROPERTYGRID);
	m_propertyGrid.SetOwner(this);
	m_propertyGrid.ShowHelp(FALSE);
	m_propertyGrid.SetViewDivider(0.35);
	m_propertyGrid.SetTheme(xtpGridThemeOffice2003);

	return 0;
}

BOOL CSpecieSettingsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CSpecieSettingsFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType,cx,cy);

	if( m_propertyGrid.GetSafeHwnd() )
	{
		setResize( &m_propertyGrid, 0, 0, cx, cy );
	}
}

LRESULT CSpecieSettingsFormView::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	if( wParam == XTP_PGN_ITEMVALUE_CHANGED )
	{
		CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
		DWORD_PTR funcId = pItem->GetConstraints()->GetConstraintAt(pItem->GetConstraints()->GetCurrent())->m_dwData;
		vecUCFunctions vecFunc;
		vecUCFunctionList vecFuncList;

		// Fill up sub-functions, reflect changes in template
		switch( pItem->GetID() )
		{
		case IDC_HOJD:
			getHeightFunctions( vecFunc, vecFuncList );
			SetupSubFunctions( pItem, m_sCalcAsHeight, vecFunc, vecFuncList, IDC_HOJDSUB );
			m_pTemplate->m_spc[m_nSpcID].funcHeight = funcId;
			m_pTemplate->m_spc[m_nSpcID].subHeight = -1;
			break;

		case IDC_VOLYM:
			getVolumeFunctions( vecFunc, vecFuncList );
			SetupSubFunctions( pItem, m_sCalcAsVolume, vecFunc, vecFuncList, IDC_VOLYMSUB );
			m_pTemplate->m_spc[m_nSpcID].funcVolume = funcId;
			m_pTemplate->m_spc[m_nSpcID].subVolume = -1;
			break;

		case IDC_BARK:
			getBarkFunctions( vecFunc, vecFuncList );
			SetupSubFunctions( pItem, m_sCalcAsBark, vecFunc, vecFuncList, IDC_BARKSUB );
			m_pTemplate->m_spc[m_nSpcID].funcBark = funcId;
			m_pTemplate->m_spc[m_nSpcID].subBark = -1;
			break;

		case IDC_VOLYMUB:
			getVolumeFunctions_ub( vecFunc, vecFuncList );
			SetupSubFunctions( pItem, m_sCalcAsVolumeUB, vecFunc, vecFuncList, IDC_VOLYMUBSUB );
			m_pTemplate->m_spc[m_nSpcID].funcVolumeUB = funcId;
			m_pTemplate->m_spc[m_nSpcID].subVolumeUB = -1;
			break;

		case IDC_HOJDSUB:
			m_pTemplate->m_spc[m_nSpcID].subHeight = pItem->GetConstraints()->GetCurrent();
			break;
		case IDC_VOLYMSUB:
			m_pTemplate->m_spc[m_nSpcID].subVolume = pItem->GetConstraints()->GetCurrent();
			break;
		case IDC_BARKSUB:
			m_pTemplate->m_spc[m_nSpcID].subBark = pItem->GetConstraints()->GetCurrent();
			break;
		case IDC_VOLYMUBSUB:
			m_pTemplate->m_spc[m_nSpcID].subVolumeUB = pItem->GetConstraints()->GetCurrent();
			break;
		}
	}

	return 0L;
}
