#pragma once

class CSpecieSettingsFormView;
class CTemplateFormView;

struct SPECIESETTINGS
{
	int funcHeight;
	int funcVolume;
	int funcBark;
	int funcVolumeUB;
	int subHeight;
	int subVolume;
	int subBark;
	int subVolumeUB;
	double h25;
	long gcrown;
};

class CThinningTemplate
{
	friend class CSpecieSettingsFormView;
	friend class CTemplateFormView;

public:
	CThinningTemplate();
	~CThinningTemplate();

	bool LoadFromBuffer(CString &buffer);
	bool LoadFromFile(CString &filename);
	bool SaveToFile(CString &filename);

protected:
	BOOL m_nearCoast;
	BOOL m_southEast;
	BOOL m_region5;
	BOOL m_partOfPlot;
	double m_dcls;
	long m_status;
	long m_hoh;
	long m_latitud;
	CString m_h100Pine;
	CString m_siH100;
	CString m_name;
	CString m_createdby;
	CString m_date;
	CString m_notes;

	SPECIESETTINGS m_spc[4];
};
