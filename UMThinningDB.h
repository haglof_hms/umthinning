#pragma once

#include <vector>

enum TREEATTRIBUTE;

// Tree attribute flags (by intention we use higher bits here so these values won't interfer with the TREEATTRIBUTE enumeration)
const DWORD TA_REMOVAL			= 16;
const DWORD TA_REMAINING		= 32;
const DWORD TA_REMOVALINROAD	= 64;

struct PLOTDATA
{
	double gyStamm;
	int antalStamm;
	double gyStubb;
	int antalStubb;
	int skStamm;
	int skRot;
	int skStRt;
	int skRota;
};

typedef std::vector<PLOTDATA> PlotDataVector;


struct AVGPLOTDATA
{
	int skTrad;
	int skStamm;
	int skRot;
	int skRota;
	double stvAvstand;
	double stvBredd;
	int stvAreal;
	double sparStracka;
	double sparDjup;
	int sparDjupProc;
};

typedef std::vector<AVGPLOTDATA> AvgPlotDataVector;


struct RESULTDATA
{
	double gyta;
	int antal;
	double dg;
};

typedef std::vector<RESULTDATA> ResultDataVector;


struct RESULTDATAEX
{
	double m3sk;
	double m3skAvg;
	double m3fub;
	double m3fubAvg;
	double gyta;
	int gytaProc;
	int antal;
	int antalProc;
	double dg;
	double gaKvot;
	double hgv;
	double h25;
};

typedef std::vector<RESULTDATAEX> ResultDataExVector;


class CUMThinningDB
{
public:
	CUMThinningDB(DB_CONNECTION_DATA &db_connection);
	~CUMThinningDB();

	bool GetThinningData(int traktId, TREEATTRIBUTE attribute, ResultDataVector &vec);
	bool GetThinningDataEx(int traktId, DWORD treetypes, ResultDataExVector &vec);
	bool GetAvgPlotData(int traktId, AVGPLOTDATA &data);
	bool GetPlotData(int traktId, PlotDataVector &vec);

protected:
	static const TCHAR SPCQUERY[4][3];
};
