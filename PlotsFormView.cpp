// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "UMThinningDB.h"
#include "UMThinningGenerics.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"

#include "PlotsFormView.h"
#include "ResLangFileReader.h"

// CPlotsFormView

IMPLEMENT_DYNCREATE(CPlotsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPlotsFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
END_MESSAGE_MAP()

CPlotsFormView::CPlotsFormView():
CXTResizeFormView(CPlotsFormView::IDD),
m_pDB(NULL)
{
}

CPlotsFormView::~CPlotsFormView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CPlotsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GROUP1, m_group1);
	DDX_Control(pDX, IDC_EDIT1, m_avlagg);
	DDX_Control(pDX, IDC_EDIT2, m_drvenhet);
	DDX_Control(pDX, IDC_EDIT3, m_datum);
	DDX_Control(pDX, IDC_EDIT4, m_ytor);
	DDX_Control(pDX, IDC_EDIT5, m_koordinat);
}

void CPlotsFormView::PopulateData()
{
	int idx = 1;
	CString str;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	PlotDataVector plots;

	m_report.ResetContent(FALSE);

	// List general data
	m_avlagg.SetWindowText(pTrakt->getTraktName());
	m_drvenhet.SetWindowText(pTrakt->getTraktPNum());
	m_datum.SetWindowText(pTrakt->getTraktDate());
	// TODO: plot count
	str.Format(_T("%d, %d"), pTrakt->getTraktLatitude(), pTrakt->getTraktLongitude());
	m_koordinat.SetWindowText(str);

	// List plots for this stand
	m_pDB->GetPlotData(pTrakt->getTraktID(), plots);
	PlotDataVector::const_iterator iter = plots.begin();
	while( iter != plots.end() )
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;
		pRec->AddItem(new CXTPReportRecordItemNumber(idx++));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->gyStamm));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->antalStamm));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->gyStubb));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->antalStubb));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->skStamm));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->skRot));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->skStRt));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->skStamm + iter->skRot + iter->skStRt));
		if( (iter->antalStamm + iter->antalStubb) > 0 )
		{
			pRec->AddItem(new CXTPReportRecordItemNumber(double(iter->skStamm) / double(iter->antalStamm + iter->antalStubb) * 100.0));
			pRec->AddItem(new CXTPReportRecordItemNumber(double(iter->skRot) / double(iter->antalStamm + iter->antalStubb) * 100.0));
			pRec->AddItem(new CXTPReportRecordItemNumber(double(iter->skRota) / double(iter->antalStamm + iter->antalStubb) * 100.0));
		}
		else
		{
			pRec->AddItem(new CXTPReportRecordItemText(_T("-")));
			pRec->AddItem(new CXTPReportRecordItemText(_T("-")));
			pRec->AddItem(new CXTPReportRecordItemText(_T("-")));
		}
		m_report.AddRecord(pRec);

		iter++;
	}

	m_report.Populate();
	m_report.UpdateWindow();
}

void CPlotsFormView::OnInitialUpdate()
{
	CXTPReportColumn *pCol;

	SetDlgItemText(IDC_STATIC1, getLangStr(STRID_AVLAGG));
	SetDlgItemText(IDC_STATIC2, getLangStr(STRID_DRVENHET));
	SetDlgItemText(IDC_STATIC3, getLangStr(STRID_DATUM));
	SetDlgItemText(IDC_STATIC4, getLangStr(STRID_ANTALYTOR));
	SetDlgItemText(IDC_STATIC5, getLangStr(STRID_KOORDINAT));

	m_avlagg.SetDisabledColor(BLACK, INFOBK);
	m_avlagg.SetReadOnly();

	m_drvenhet.SetDisabledColor(BLACK, INFOBK);
	m_drvenhet.SetReadOnly();

	m_datum.SetDisabledColor(BLACK, INFOBK);
	m_datum.SetReadOnly();

	m_ytor.SetDisabledColor(BLACK, INFOBK);
	m_ytor.SetReadOnly();

	m_koordinat.SetDisabledColor(BLACK, INFOBK);
	m_koordinat.SetReadOnly();


	// Set up report
	if( m_report.Create(this, IDC_REPORTPLOTS, TRUE, FALSE) )
	{
		pCol = m_report.AddColumn(new CXTPReportColumn(0, getLangStr(STRID_YTORCOL1), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report.AddColumn(new CXTPReportColumn(1, getLangStr(STRID_YTORCOL2), 100));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(2, getLangStr(STRID_YTORCOL3), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(3, getLangStr(STRID_YTORCOL4), 100));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(4, getLangStr(STRID_YTORCOL5), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(5, getLangStr(STRID_YTORCOL6), 100));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(6, getLangStr(STRID_YTORCOL7), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(7, getLangStr(STRID_YTORCOL8), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(8, getLangStr(STRID_YTORCOL9), 100));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(9, getLangStr(STRID_YTORCOL10), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(10, getLangStr(STRID_YTORCOL11), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(11, getLangStr(STRID_YTORCOL12), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
	}

	m_report.ModifyStyle(0, WS_BORDER);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());
}

BOOL CPlotsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		m_pDB = new CUMThinningDB(*((DB_CONNECTION_DATA*)pData->lpData));
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CPlotsFormView::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame*)GetParentFrame();
	if( pFrame )
	{
		pFrame->setToolbarItems( FALSE, FALSE, FALSE );
	}
}

void CPlotsFormView::OnSize(UINT nType, int cx, int cy)
{
	RECT rect;

	GetClientRect(&rect);

	if( m_report.GetSafeHwnd() )
	{
		setResize( &m_report, 10, 100, rect.right - 20, rect.bottom - 110 );
	}
}
