#pragma once

#include "Resource.h"

class CResultsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CResultsFormView)

protected:
	CResultsFormView();           // protected constructor used by dynamic creation
	virtual ~CResultsFormView();

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon, int id);

	CMyTabControl m_wndTabControl;

public:
	enum { IDD = IDD_FORMVIEW };

	void PopulateData();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();

	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};
