#include "stdafx.h"
#include "defines.h"
#include "TemplateFrame.h"

IMPLEMENT_DYNCREATE(CTemplateFrame, CChildFrameBase)

CTemplateFrame::CTemplateFrame():
m_bFirstShow(TRUE)
{
}

CTemplateFrame::~CTemplateFrame()
{
}

BEGIN_MESSAGE_MAP(CTemplateFrame, CChildFrameBase)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
END_MESSAGE_MAP()


LRESULT CTemplateFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	switch( wParam )
	{
	case ID_DBNAVIG_LIST:
		//showFormView( IDD_TRAKTSELECT, m_sLangFN );
		break;

	default:
		// Forward message to view(s)
		CDocument *pDoc = GetActiveDocument();
		if( pDoc )
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while( pos )
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage( MSG_IN_SUITE, wParam, lParam );
			}
		}
		break;
	}

	return 0L;
}

void CTemplateFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DELETE_ITEM, FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_LIST, FALSE);

	CMDIChildWnd::OnClose();
}

void CTemplateFrame::OnSetFocus(CWnd *pOldWnd)
{
	// Send messages to HMSShell, enable buttons on toolbar
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_SAVE_ITEM, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DELETE_ITEM, TRUE);

	// Send messages to HMSShell, enable DBNavigation buttons on DBNavigation toolbar
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_START, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_PREV, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_NEXT, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_END, TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_LIST, TRUE);
}

void CTemplateFrame::OnDestroy()
{
	// Save window position
	CString buf; buf.Format(_T("%s\\%s"), REG_ROOT, REG_PLACEMENT_TEMPLATEFRAME);
	SavePlacement(this, buf);

	CMDIChildWnd::OnDestroy();
}

void CTemplateFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	// Load window position
    if( bShow && !IsWindowVisible() && m_bFirstShow )
    {
		m_bFirstShow = false; // Set this flag before loading window placement!

		CString buf; buf.Format(_T("%s\\%s"), REG_ROOT, REG_PLACEMENT_TEMPLATEFRAME);
		LoadPlacement(this, buf);
    }
}
