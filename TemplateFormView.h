#pragma once

#include "Resource.h"
#include "ThinningTemplate.h"

class CTemplateFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTemplateFormView)

public:
	enum { IDD = IDD_FORMVIEW14 };

	virtual void OnInitialUpdate();

protected:
	CTemplateFormView();           // protected constructor used by dynamic creation
	virtual ~CTemplateFormView();

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	void ExportToFile();

	CThinningTemplate m_template;

	CXTResizeGroupBox m_group1;
	CXTResizeGroupBox m_group2;
	CMyPropertyGrid m_propertyGrid;
	CMyTabControl m_tabControl;
	CMyComboBox m_status;
	CMyExtEdit m_name;
	CMyExtEdit m_createdby;
	CMyExtEdit m_date;
	CMyExtEdit m_notes;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnGridNotify(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTextChanged(UINT nID);
	afx_msg void OnStatusChanged();
	DECLARE_MESSAGE_MAP()
};
