#pragma once

#include "stdafx.h"

const unsigned char UTF16_BOM[] = { unsigned char(0xFF), unsigned char(0xFE) };

const LPCTSTR PROGRAM_NAMETHINNING = _T("UMThinning");

const TCHAR g_specie[4][10] = { {_T("Tall")}, {_T("Gran")}, {_T("L�v")}, {_T("�.L�v")} };

enum TREEATTRIBUTE { REMOVAL = 1, REMAINING, REMOVALINROAD };

CString getLangStr(DWORD resid);

typedef void (CXTPPropertyGridItem::*ITEMFUNCTIONPTR)();
void _DoCollapseExpand(CXTPPropertyGridItems* pItems, ITEMFUNCTIONPTR pFunction);
