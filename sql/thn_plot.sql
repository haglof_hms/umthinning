USE [peter1]
GO

/****** Object:  Table [dbo].[thn_plot]    Script Date: 06/25/2009 14:13:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[thn_plot](
	[traktId] [int] NOT NULL,
	[ytId] [int] NOT NULL,
	[gyStamm] [float] NULL,
	[gyStubb] [float] NULL,
	[antalStamm] [int] NULL,
	[antalStubb] [int] NULL,
	[skadorStammZon1] [int] NULL,
	[skadorStammZon2] [int] NULL,
	[skadorRotZon1] [int] NULL,
	[skadorRotZon2] [int] NULL,
	[skadorStRtZon1] [int] NULL,
	[skadorStRtZon2] [int] NULL,
	[skadorRota] [int] NULL,
	[stvAvst1] [int] NULL,
	[stvAvst2] [int] NULL,
	[stvBredd1] [int] NULL,
	[stvBredd2] [int] NULL,
	[sparLangd] [int] NULL,
	[sparDjup1] [int] NULL,
	[sparDjup2] [int] NULL,
 CONSTRAINT [PK_thn_plot] PRIMARY KEY CLUSTERED 
(
	[traktId] ASC,
	[ytId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[thn_plot]  WITH CHECK ADD  CONSTRAINT [FK_thn_plot_esti_trakt_plot_table] FOREIGN KEY([ytId], [traktId])
REFERENCES [dbo].[esti_trakt_plot_table] ([tplot_id], [tplot_trakt_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
