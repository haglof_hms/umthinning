// UMThinning.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"


HINSTANCE g_hInstance = NULL;

static AFX_EXTENSION_MODULE UMThinningDLL = { NULL, NULL };

extern "C" void AFX_EXT_API InitModule(CWinApp *app, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMThinningDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;

	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	// Doc templates
	AfxGetApp()->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW1,
			RUNTIME_CLASS(CMDILicenseFrameDoc),
			RUNTIME_CLASS(CMDIStandEntryFormFrame),
			RUNTIME_CLASS(CMDITabbedView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW1, suite, sLangFN, TRUE));


	// Get version information; 060803 p�d
	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(g_hInstance,VER_COPYRIGHT);
	sCompany	= getVersionInfo(g_hInstance,VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		2/* Set to 1 to indicate a SUITE; 2 indicates a  User Module*/,
		sLangFN.GetBuffer(),
		sVersion.GetBuffer(),
		sCopyright.GetBuffer(),
		sCompany.GetBuffer()));
}

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		g_hInstance = hInstance;

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMThinningDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMThinningDLL);
	}

	return 1;   // ok
}
