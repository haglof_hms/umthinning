#pragma once

#include "Resource.h"

class CUMThinningDB;

class CPreviousFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPreviousFormView)

protected:
	CPreviousFormView();           // protected constructor used by dynamic creation
	virtual ~CPreviousFormView();

	CUMThinningDB *m_pDB;
	CMyReportCtrl m_report;

public:
	enum { IDD = IDD_FORMVIEW11 };

	void PopulateData();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();

	CXTResizeGroupBox m_group1;
	CMyExtEdit m_avlagg;
	CMyExtEdit m_drvenhet;
	CMyExtEdit m_datum;
	CMyExtEdit m_medelhojd;
	CMyExtEdit m_koordinat;

	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	DECLARE_MESSAGE_MAP()
};
