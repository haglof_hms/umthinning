// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "UMThinningDB.h"
#include "UMThinningGenerics.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"

#include "RemovalFormView.h"
#include "ResLangFileReader.h"

// CRemovalFormView

IMPLEMENT_DYNCREATE(CRemovalFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CRemovalFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
END_MESSAGE_MAP()

CRemovalFormView::CRemovalFormView():
CXTResizeFormView(CRemovalFormView::IDD),
m_pDB(NULL)
{
}

CRemovalFormView::~CRemovalFormView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CRemovalFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GROUP1, m_group1);
	DDX_Control(pDX, IDC_EDIT1, m_avlagg);
	DDX_Control(pDX, IDC_EDIT2, m_drvenhet);
	DDX_Control(pDX, IDC_EDIT3, m_datum);
	DDX_Control(pDX, IDC_EDIT4, m_koordinat);
}

void CRemovalFormView::PopulateData()
{
	int idx;
	CString str;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	ResultDataVector rd;
	ResultDataExVector rex;

	m_report1.ResetContent(FALSE);
	m_report2.ResetContent(FALSE);
	m_report3.ResetContent(FALSE);

	// List general data
	m_avlagg.SetWindowText(pTrakt->getTraktName());
	m_drvenhet.SetWindowText(pTrakt->getTraktPNum());
	m_datum.SetWindowText(pTrakt->getTraktDate());
	str.Format(_T("%d, %d"), pTrakt->getTraktLatitude(), pTrakt->getTraktLongitude());
	m_koordinat.SetWindowText(str);

	// List "removal inside road" for this stand
	idx = 0;
	m_pDB->GetThinningData(pTrakt->getTraktID(), REMOVALINROAD, rd);
	ResultDataVector::const_iterator iter1 = rd.begin();
	while( iter1 != rd.end() )
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;
		pRec->AddItem(new CXTPReportRecordItemText(g_specie[idx++]));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter1->gyta));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter1->antal));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter1->dg));
		m_report1.AddRecord(pRec);

		iter1++;
	}

	// List "removal outside road" for this stand
	idx = 0; rd.clear();
	m_pDB->GetThinningData(pTrakt->getTraktID(), REMOVAL, rd);
	ResultDataVector::const_iterator iter2 = rd.begin();
	while( iter2 != rd.end() )
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;
		pRec->AddItem(new CXTPReportRecordItemText(g_specie[idx++]));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter2->gyta));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter2->antal));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter2->dg));
		m_report2.AddRecord(pRec);

		iter2++;
	}

	// List totals for this stand
	idx = 0;
	m_pDB->GetThinningDataEx(pTrakt->getTraktID(), TA_REMOVAL | TA_REMOVALINROAD, rex);
	ResultDataExVector::const_iterator iter3 = rex.begin();
	while( iter3 != rex.end() )
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;
		pRec->AddItem(new CXTPReportRecordItemText(g_specie[idx++]));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->m3sk));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->m3skAvg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->m3fub));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->m3fubAvg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->gyta));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->gytaProc));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->antal));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->antalProc));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->dg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter3->gaKvot));
		m_report3.AddRecord(pRec);

		iter3++;
	}

	m_report1.Populate();
	m_report2.Populate();
	m_report3.Populate();
	m_report1.UpdateWindow();
	m_report2.UpdateWindow();
	m_report3.UpdateWindow();
}

void CRemovalFormView::OnInitialUpdate()
{
	CXTPReportColumn *pCol;

	SetDlgItemText(IDC_STATIC1, getLangStr(STRID_AVLAGG));
	SetDlgItemText(IDC_STATIC2, getLangStr(STRID_DRVENHET));
	SetDlgItemText(IDC_STATIC3, getLangStr(STRID_DATUM));
	SetDlgItemText(IDC_STATIC4, getLangStr(STRID_KOORDINAT));
	SetDlgItemText(IDC_STATIC5, getLangStr(STRID_ISTV));
	SetDlgItemText(IDC_STATIC6, getLangStr(STRID_MELLANSTV));
	SetDlgItemText(IDC_STATIC7, getLangStr(STRID_TOTALTUTTAG));

	m_avlagg.SetDisabledColor(BLACK, INFOBK);
	m_avlagg.SetReadOnly();

	m_drvenhet.SetDisabledColor(BLACK, INFOBK);
	m_drvenhet.SetReadOnly();

	m_datum.SetDisabledColor(BLACK, INFOBK);
	m_datum.SetReadOnly();

	m_koordinat.SetDisabledColor(BLACK, INFOBK);
	m_koordinat.SetReadOnly();


	// Set up reports
	if( m_report1.Create(this, IDC_REPORTPLOTS, FALSE, FALSE) )
	{
		pCol = m_report1.AddColumn(new CXTPReportColumn(0, _T(""), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report1.AddColumn(new CXTPReportColumn(1, getLangStr(STRID_ISTVGYTA), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report1.AddColumn(new CXTPReportColumn(2, getLangStr(STRID_ISTVANTAL), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report1.AddColumn(new CXTPReportColumn(3, getLangStr(STRID_ISTVDG), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
	}

	if( m_report2.Create(this, IDC_REPORTPLOTS, FALSE, FALSE) )
	{
		pCol = m_report2.AddColumn(new CXTPReportColumn(0, _T(""), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report2.AddColumn(new CXTPReportColumn(1, getLangStr(STRID_MSTVGYTA), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report2.AddColumn(new CXTPReportColumn(2, getLangStr(STRID_MSTVANTAL), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report2.AddColumn(new CXTPReportColumn(3,getLangStr(STRID_MSTVDG), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
	}

	if( m_report3.Create(this, IDC_REPORTPLOTS, FALSE, FALSE) )
	{
		pCol = m_report3.AddColumn(new CXTPReportColumn(0, _T(""), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report3.AddColumn(new CXTPReportColumn(1, getLangStr(STRID_UTTAGCOL1), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(2, getLangStr(STRID_UTTAGCOL2), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(3, getLangStr(STRID_UTTAGCOL3), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(4, getLangStr(STRID_UTTAGCOL4), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(5, getLangStr(STRID_UTTAGCOL5), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(6, getLangStr(STRID_UTTAGCOL6), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(7, getLangStr(STRID_UTTAGCOL7), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(8, getLangStr(STRID_UTTAGCOL8), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(9, getLangStr(STRID_UTTAGCOL9), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report3.AddColumn(new CXTPReportColumn(10, getLangStr(STRID_UTTAGCOL10), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
	}

	m_report1.ModifyStyle(0, WS_BORDER);
	m_report2.ModifyStyle(0, WS_BORDER);
	m_report3.ModifyStyle(0, WS_BORDER);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());
}

BOOL CRemovalFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		m_pDB = new CUMThinningDB(*((DB_CONNECTION_DATA*)pData->lpData));
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CRemovalFormView::OnSize(UINT nType, int cx, int cy)
{
	RECT rect;

	GetClientRect(&rect);

	if( m_report1.GetSafeHwnd() )
	{
		setResize( &m_report1, 10, 130, 320, 110 );
	}
	if( m_report2.GetSafeHwnd() )
	{
		setResize( &m_report2, 479, 130, 320, 110 );
	}
	if( m_report3.GetSafeHwnd() )
	{
		setResize( &m_report3, 10, 282, 790, 110 );
	}
}
