#pragma once

const LPCTSTR REG_PLACEMENT_TEMPLATEFRAME = _T("UMThinning\\TemplateFrame\\Placement");

#define IDC_REPORTPLOTS			0x80001
#define IDC_FORMVIEWBEFORE		0x80002
#define IDC_FORMVIEWREMAINING	0x80003
#define IDC_FORMVIEWREMOVAL		0x80004
#define IDC_FORMVIEWDAMAGES		0x80005
#define IDC_H25					0x80006
#define IDC_GCROWN				0x80007
#define IDC_ATCOAST				0x80008			
#define IDC_SOUTHEAST			0x80009
#define IDC_REGION5				0x8000a
#define IDC_PART_OF_PLOT		0x8000b
#define IDC_SIH100_PINE			0x8000c
#define IDC_DCLS				0x8000d
#define IDC_HOH					0x8000e
#define IDC_LATITUD				0x8000f
#define IDC_H100				0x80010
#define IDC_HOJD				0x80011
#define IDC_VOLYM				0x80012
#define IDC_BARK				0x80013
#define IDC_VOLYMUB				0x80014
#define IDC_HOJDSUB				0x80015
#define IDC_VOLYMSUB			0x80016
#define IDC_BARKSUB				0x80017
#define IDC_VOLYMUBSUB			0x80018

// Form captions
const int STRID_FORMYTOR		= 401;
const int STRID_FORMRESULTAT	= 402;
const int STRID_FORMFORE		= 403;
const int STRID_FORMKVARVARANDE	= 404;
const int STRID_FORMUTTAG		= 405;
const int STRID_FORMSKADOR		= 406;

const int STRID_AVLAGG			= 410;
const int STRID_DRVENHET		= 411;
const int STRID_DATUM			= 412;
const int STRID_KOORDINAT		= 413;
const int STRID_TALL			= 414;
const int STRID_GRAN			= 415;
const int STRID_LOV				= 416;
const int STRID_OVRLOV			= 417;

// Landing (Avl�gg) form
const int STRID_FORVALTNING		= 420;
const int STRID_DISTRIKT		= 421;
const int STRID_MASKINLAG		= 422;
const int STRID_PRODLEDARE		= 423;
const int STRID_METOD			= 424;
const int STRID_AREAL			= 425;
const int STRID_SI				= 426;

// Plot (Ytor) form
const int STRID_YTORCOL1		= 500;
const int STRID_YTORCOL2		= 501;
const int STRID_YTORCOL3		= 502;
const int STRID_YTORCOL4		= 503;
const int STRID_YTORCOL5		= 504;
const int STRID_YTORCOL6		= 505;
const int STRID_YTORCOL7		= 506;
const int STRID_YTORCOL8		= 507;
const int STRID_YTORCOL9		= 508;
const int STRID_YTORCOL10		= 509;
const int STRID_YTORCOL11		= 510;
const int STRID_YTORCOL12		= 511;
const int STRID_ANTALYTOR		= 512;

// Result (Resultat) form
const int STRID_RESULTATCOL1	= 600;
const int STRID_RESULTATCOL2	= 601;
const int STRID_RESULTATCOL3	= 602;
const int STRID_RESULTATCOL4	= 603;
const int STRID_RESULTATCOL5	= 604;
const int STRID_RESULTATCOL6	= 605;
const int STRID_RESULTATCOL7	= 606;
const int STRID_RESULTATCOL8	= 607;
const int STRID_MEDELHOJD		= 608;

// Remaining (Kvarvarande) form
const int STRID_KVARVCOL1		= 700;
const int STRID_KVARVCOL2		= 701;
const int STRID_KVARVCOL3		= 702;
const int STRID_KVARVCOL4		= 703;
const int STRID_KVARVCOL5		= 704;
const int STRID_KVARVCOL6		= 705;
const int STRID_KVARVCOL7		= 706;
const int STRID_KVARVCOL8		= 707;
const int STRID_KVARVCOL9		= 708;

// Removal (Uttag) form
const int STRID_ISTV			= 800;
const int STRID_MELLANSTV		= 801;
const int STRID_TOTALTUTTAG		= 802;
const int STRID_ISTVGYTA		= 803;
const int STRID_ISTVANTAL		= 804;
const int STRID_ISTVDG			= 805;
const int STRID_MSTVGYTA		= 806;
const int STRID_MSTVANTAL		= 807;
const int STRID_MSTVDG			= 808;
const int STRID_UTTAGCOL1		= 809;
const int STRID_UTTAGCOL2		= 810;
const int STRID_UTTAGCOL3		= 811;
const int STRID_UTTAGCOL4		= 812;
const int STRID_UTTAGCOL5		= 813;
const int STRID_UTTAGCOL6		= 814;
const int STRID_UTTAGCOL7		= 815;
const int STRID_UTTAGCOL8		= 816;
const int STRID_UTTAGCOL9		= 817;
const int STRID_UTTAGCOL10		= 818;

// Damages (Skador) form
const int STRID_SKTRAD			= 900;
const int STRID_SKSTAM			= 901;
const int STRID_SKROTTER		= 902;
const int STRID_ROTA			= 903;
const int STRID_STVAVSTAND		= 904;
const int STRID_STVBREDD		= 905;
const int STRID_STVAREAL		= 906;
const int STRID_SPARSTRACKA		= 907;
const int STRID_SPARDJUP		= 908;
const int STRID_SPARDJUPPROC	= 909;
const int STRID_SKADOR			= 910;
const int STRID_TOTALTSKADOR	= 911;
const int STRID_ZON1			= 912;
const int STRID_ZON2			= 913;
const int STRID_STAM			= 914;
const int STRID_ROT				= 915;
const int STRID_ROTSTAM			= 916;
