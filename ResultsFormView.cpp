// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "UMThinningGenerics.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"

#include "SpecieDataDialog.h"
#include "PreviousFormView.h"
#include "RemainingFormView.h"
#include "RemovalFormView.h"
#include "DamagesFormView.h"
#include "ResultsFormView.h"
#include "ResLangFileReader.h"

// CResultsFormView

IMPLEMENT_DYNCREATE(CResultsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CResultsFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
END_MESSAGE_MAP()

CResultsFormView::CResultsFormView():
CXTResizeFormView(CResultsFormView::IDD)
{
}

CResultsFormView::~CResultsFormView()
{
}

void CResultsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

void CResultsFormView::PopulateData()
{
	CXTPTabManagerItem *tabItem;

	// Previous		"F�re"
	tabItem = m_wndTabControl.getTabPage(0);
	if( tabItem )
	{
		CPreviousFormView *pView = DYNAMIC_DOWNCAST(CPreviousFormView, CWnd::FromHandle(tabItem->GetHandle()));
		if( pView )
		{
			pView->PopulateData();
		}
	}

	// Remaining	"Kvarvarande"
	tabItem = m_wndTabControl.getTabPage(1);
	if( tabItem )
	{
		CRemainingFormView *pView = DYNAMIC_DOWNCAST(CRemainingFormView, CWnd::FromHandle(tabItem->GetHandle()));
		if( pView )
		{
			pView->PopulateData();
		}
	}

	// Removal		"Uttag"
	tabItem = m_wndTabControl.getTabPage(2);
	if( tabItem )
	{
		CRemovalFormView *pView = DYNAMIC_DOWNCAST(CRemovalFormView, CWnd::FromHandle(tabItem->GetHandle()));
		if( pView )
		{
			pView->PopulateData();
		}
	}

	// Damages		"Skador"
	tabItem = m_wndTabControl.getTabPage(3);
	if( tabItem )
	{
		CDamagesFormView *pView = DYNAMIC_DOWNCAST(CDamagesFormView, CWnd::FromHandle(tabItem->GetHandle()));
		if( pView )
		{
			pView->PopulateData();
		}
	}
}

void CResultsFormView::OnInitialUpdate()
{
	if( m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_TABCONTROL1) )
	{
		m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
		m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
		m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
		m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
		m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);
	}

	AddView(RUNTIME_CLASS(CPreviousFormView), getLangStr(STRID_FORMFORE), 3, IDC_FORMVIEWBEFORE);
	AddView(RUNTIME_CLASS(CRemainingFormView), getLangStr(STRID_FORMKVARVARANDE), 3, IDC_FORMVIEWREMAINING);
	AddView(RUNTIME_CLASS(CRemovalFormView), getLangStr(STRID_FORMUTTAG), 3, IDC_FORMVIEWREMOVAL);
	AddView(RUNTIME_CLASS(CDamagesFormView), getLangStr(STRID_FORMSKADOR), 3, IDC_FORMVIEWDAMAGES);
}

void CResultsFormView::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	CMDIStandEntryFormFrame *pFrame = (CMDIStandEntryFormFrame*)GetParentFrame();
	if( pFrame )
	{
		pFrame->setToolbarItems( FALSE, FALSE, FALSE );
	}
}

void CResultsFormView::OnSize(UINT nType, int cx, int cy)
{
	RECT rect;

	GetClientRect(&rect);
	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(10, 10, rect.right - 20, rect.bottom - 70);
	}
}

BOOL CResultsFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon, int id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CSpecieDataFormView*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,rect, &m_wndTabControl, id, &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	int nTab = m_wndTabControl.getNumOfTabPages();
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

//	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}
