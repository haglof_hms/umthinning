// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "UMThinningDB.h"
#include "UMThinningGenerics.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"

#include "DamagesFormView.h"
#include "ResLangFileReader.h"

// CDamagesFormView

IMPLEMENT_DYNCREATE(CDamagesFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CDamagesFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
END_MESSAGE_MAP()

CDamagesFormView::CDamagesFormView():
CXTResizeFormView(CDamagesFormView::IDD),
m_pDB(NULL)
{
}

CDamagesFormView::~CDamagesFormView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CDamagesFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GROUP1, m_group1);
	DDX_Control(pDX, IDC_EDIT1, m_avlagg);
	DDX_Control(pDX, IDC_EDIT2, m_drvenhet);
	DDX_Control(pDX, IDC_EDIT3, m_datum);
	DDX_Control(pDX, IDC_EDIT4, m_koordinat);
}

void CDamagesFormView::PopulateData()
{
	CString str;
	CXTPReportRecord *pRec;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	AVGPLOTDATA data;

	m_report1.ResetContent(FALSE);
	m_report2.ResetContent(FALSE);

	// List general data
	m_avlagg.SetWindowText(pTrakt->getTraktName());
	m_drvenhet.SetWindowText(pTrakt->getTraktPNum());
	m_datum.SetWindowText(pTrakt->getTraktDate());
	str.Format(_T("%d, %d"), pTrakt->getTraktLatitude(), pTrakt->getTraktLongitude());
	m_koordinat.SetWindowText(str);

	// List damage statistics
	m_pDB->GetAvgPlotData(pTrakt->getTraktID(), data);
	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_SKTRAD)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.skTrad));
	m_report1.AddRecord(pRec);
	
	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_SKSTAM)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.skStamm));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_SKROTTER)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.skRot));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_ROTA)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.skRota));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_STVAVSTAND)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.stvAvstand));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_STVBREDD)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.stvBredd));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_STVAREAL)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.stvAreal));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_SPARSTRACKA)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.sparStracka));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_SPARDJUP)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.sparDjup));
	m_report1.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_SPARDJUPPROC)));
	pRec->AddItem(new CXTPReportRecordItemNumber(data.sparDjupProc));
	m_report1.AddRecord(pRec);


	// List damages
	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_STAM)));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	m_report2.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_ROT)));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	m_report2.AddRecord(pRec);

	pRec = new CXTPReportRecord;
	pRec->AddItem(new CXTPReportRecordItemText(getLangStr(STRID_ROTSTAM)));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	pRec->AddItem(new CXTPReportRecordItemNumber(0));
	m_report2.AddRecord(pRec);

	m_report1.Populate();
	m_report2.Populate();
	m_report1.UpdateWindow();
	m_report2.UpdateWindow();
}

void CDamagesFormView::OnInitialUpdate()
{
	CXTPReportColumn *pCol;

	SetDlgItemText(IDC_STATIC1, getLangStr(STRID_AVLAGG));
	SetDlgItemText(IDC_STATIC2, getLangStr(STRID_DRVENHET));
	SetDlgItemText(IDC_STATIC3, getLangStr(STRID_DATUM));
	SetDlgItemText(IDC_STATIC4, getLangStr(STRID_KOORDINAT));

	m_avlagg.SetDisabledColor(BLACK, INFOBK);
	m_avlagg.SetReadOnly();

	m_drvenhet.SetDisabledColor(BLACK, INFOBK);
	m_drvenhet.SetReadOnly();

	m_datum.SetDisabledColor(BLACK, INFOBK);
	m_datum.SetReadOnly();

	m_koordinat.SetDisabledColor(BLACK, INFOBK);
	m_koordinat.SetReadOnly();


	// Set up reports
	if( m_report1.Create(this, IDC_REPORTPLOTS, FALSE, FALSE) )
	{
		pCol = m_report1.AddColumn(new CXTPReportColumn(0, _T(""), 80));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report1.AddColumn(new CXTPReportColumn(1, _T(""), 20));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		m_report1.ShowHeader(FALSE);
	}

	if( m_report2.Create(this, IDC_REPORTPLOTS, FALSE, FALSE) )
	{
		pCol = m_report2.AddColumn(new CXTPReportColumn(0, getLangStr(STRID_SKADOR), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report2.AddColumn(new CXTPReportColumn(1, getLangStr(STRID_TOTALTSKADOR), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report2.AddColumn(new CXTPReportColumn(2, getLangStr(STRID_ZON1), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report2.AddColumn(new CXTPReportColumn(3, getLangStr(STRID_ZON2), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
	}

	m_report1.ModifyStyle(0, WS_BORDER);
	m_report2.ModifyStyle(0, WS_BORDER);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());
}

BOOL CDamagesFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		m_pDB = new CUMThinningDB(*((DB_CONNECTION_DATA*)pData->lpData));
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CDamagesFormView::OnSize(UINT nType, int cx, int cy)
{
	RECT rect;

	GetClientRect(&rect);

	if( m_report1.GetSafeHwnd() )
	{
		setResize( &m_report1, 10, 130, 280, 200 );
	}
	if( m_report2.GetSafeHwnd() )
	{
		setResize( &m_report2, 360, 240, 330, 90 );
	}
}
