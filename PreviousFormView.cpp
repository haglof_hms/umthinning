// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "UMThinningDB.h"
#include "UMThinningGenerics.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"
#include "SpecieDataDialog.h"

#include "PreviousFormView.h"
#include "ResLangFileReader.h"

// CPreviousFormView

IMPLEMENT_DYNCREATE(CPreviousFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPreviousFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_NOTIFY(NM_CLICK, IDC_REPORTPLOTS, OnReportItemClick)
END_MESSAGE_MAP()

CPreviousFormView::CPreviousFormView():
CXTResizeFormView(CPreviousFormView::IDD),
m_pDB(NULL)
{
}

CPreviousFormView::~CPreviousFormView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CPreviousFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GROUP1, m_group1);
	DDX_Control(pDX, IDC_EDIT1, m_avlagg);
	DDX_Control(pDX, IDC_EDIT2, m_drvenhet);
	DDX_Control(pDX, IDC_EDIT3, m_datum);
	DDX_Control(pDX, IDC_EDIT4, m_medelhojd);
	DDX_Control(pDX, IDC_EDIT5, m_koordinat);
}

void CPreviousFormView::PopulateData()
{
	int idx;
	CString str;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	ResultDataExVector rex;

	m_report.ResetContent(FALSE);

	// List general data
	m_avlagg.SetWindowText(pTrakt->getTraktName());
	m_drvenhet.SetWindowText(pTrakt->getTraktPNum());
	m_datum.SetWindowText(pTrakt->getTraktDate());
	// TODO: avg height
	str.Format(_T("%d, %d"), pTrakt->getTraktLatitude(), pTrakt->getTraktLongitude());
	m_koordinat.SetWindowText(str);

	// List data for this stand
	idx = 0;
	m_pDB->GetThinningDataEx(pTrakt->getTraktID(), TA_REMOVAL | TA_REMAINING | TA_REMOVALINROAD, rex);
	ResultDataExVector::const_iterator iter = rex.begin();
	while( iter != rex.end() )
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;
		pRec->AddItem(new CXTPReportRecordItemText(g_specie[idx++]));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3sk));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3skAvg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3fub));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3fubAvg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->gyta));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->dg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->antal));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->gaKvot));
		m_report.AddRecord(pRec);

		iter++;
	}

	m_report.Populate();
	m_report.UpdateWindow();
}

void CPreviousFormView::OnInitialUpdate()
{
	CXTPReportColumn *pCol;

	SetDlgItemText(IDC_STATIC1, getLangStr(STRID_AVLAGG));
	SetDlgItemText(IDC_STATIC2, getLangStr(STRID_DRVENHET));
	SetDlgItemText(IDC_STATIC3, getLangStr(STRID_DATUM));
	SetDlgItemText(IDC_STATIC4, getLangStr(STRID_MEDELHOJD));
	SetDlgItemText(IDC_STATIC5, getLangStr(STRID_KOORDINAT));

	m_avlagg.SetDisabledColor(BLACK, INFOBK);
	m_avlagg.SetReadOnly();

	m_drvenhet.SetDisabledColor(BLACK, INFOBK);
	m_drvenhet.SetReadOnly();

	m_datum.SetDisabledColor(BLACK, INFOBK);
	m_datum.SetReadOnly();

	m_medelhojd.SetDisabledColor(BLACK, INFOBK);
	m_medelhojd.SetReadOnly();

	m_koordinat.SetDisabledColor(BLACK, INFOBK);
	m_koordinat.SetReadOnly();


	// Set up report
	if( m_report.Create(this, IDC_REPORTPLOTS, TRUE, FALSE) )
	{
		pCol = m_report.AddColumn(new CXTPReportColumn(0, _T(""), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report.AddColumn(new CXTPReportColumn(1, getLangStr(STRID_RESULTATCOL1), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(2, getLangStr(STRID_RESULTATCOL2), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(3, getLangStr(STRID_RESULTATCOL3), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(4, getLangStr(STRID_RESULTATCOL4), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(5, getLangStr(STRID_RESULTATCOL5), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(6, getLangStr(STRID_RESULTATCOL6), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(7,getLangStr(STRID_RESULTATCOL7), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(8, getLangStr(STRID_RESULTATCOL8), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
	}

	m_report.ModifyStyle(0, WS_BORDER);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());
}

BOOL CPreviousFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		m_pDB = new CUMThinningDB(*((DB_CONNECTION_DATA*)pData->lpData));
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CPreviousFormView::OnSize(UINT nType, int cx, int cy)
{
	RECT rect;

	GetClientRect(&rect);

	if( m_report.GetSafeHwnd() )
	{
		setResize( &m_report, 10, 100, 790, 110 );
	}
}

void CPreviousFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	if( pItemNotify )
	{
		CXTPReportColumn *pCol = pItemNotify->pColumn;
		if( pCol && pCol->GetItemIndex() == 0 )
		{
			if( pItemNotify->pItem )
			{
				CString str; str.Format(_T("%d"), pItemNotify->pItem->GetIndex());
				AfxMessageBox(str);

				CString langFile; langFile.Format( _T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT );
				showFormView(IDD_FORMVIEW8, langFile);
				CSpecieDataFormView *pView = (CSpecieDataFormView*)getFormViewByID(IDD_FORMVIEW8);
				if (pView != NULL)
				{
					pView->setAction(UPD_ITEM);
					pView->setSpecificLanguage();
					//pView->setSpecieID(trakt_data.getSpecieID());
					//pView->setSpecieName(trakt_data.getSpecieName());
					pView->populateData();
					pView->setupPropertyGrid();
				}
			}
		}
	}
}
