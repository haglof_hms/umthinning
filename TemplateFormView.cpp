#include "stdafx.h"
#include "defines.h"
#include "Resource.h"
#include "ResLangFileReader.h"
#include "SpecieSettingsFormView.h"
#include "StandEntryDoc.h"
#include "TemplateFormView.h"
#include "UMThinningGenerics.h"

IMPLEMENT_DYNCREATE(CTemplateFormView, CXTResizeFormView)

CTemplateFormView::CTemplateFormView():
CXTResizeFormView(CTemplateFormView::IDD)
{
}

CTemplateFormView::~CTemplateFormView()
{
}

BEGIN_MESSAGE_MAP(CTemplateFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO1, OnStatusChanged)
	ON_CONTROL_RANGE(EN_CHANGE, IDC_EDIT1, IDC_EDIT4, OnTextChanged)
	ON_MESSAGE(MSG_IN_SUITE, OnMessageFromSuite)
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)
END_MESSAGE_MAP()

void CTemplateFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GROUP1, m_group1);
	DDX_Control(pDX, IDC_GROUP2, m_group2);
	DDX_Control(pDX, IDC_COMBO1, m_status);
	DDX_Control(pDX, IDC_EDIT1, m_name);
	DDX_Control(pDX, IDC_EDIT2, m_createdby);
	DDX_Control(pDX, IDC_EDIT3, m_date);
	DDX_Control(pDX, IDC_EDIT4, m_notes);
}

void CTemplateFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	CXTPPropertyGridItem *pCategory;

	RLFReader *xml = new RLFReader;
	CString langFile = getLanguageFN(getLanguageDir(), _T("UMTemplates"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
	xml->Load(langFile);

	// Generella data
	GetDlgItem(IDC_GROUP2)->SetWindowText(xml->str(100));
	GetDlgItem(IDC_STATIC5)->SetWindowText(xml->str(1100));
	GetDlgItem(IDC_STATIC1)->SetWindowText(xml->str(1001));
	GetDlgItem(IDC_STATIC2)->SetWindowText(xml->str(1002));
	GetDlgItem(IDC_STATIC3)->SetWindowText(xml->str(1003));
	GetDlgItem(IDC_STATIC4)->SetWindowText(xml->str(216));
	GetDlgItem(IDC_STATIC6)->SetWindowText(xml->str(1103));
	m_status.AddString(xml->str(1101));
	m_status.AddString(xml->str(1102));

	// �vriga inst�llningar
	pCategory = m_propertyGrid.AddCategory(xml->str(203));

	// Diameterklass
	CXTPPropertyGridItemDouble *pItem1 = (CXTPPropertyGridItemDouble*)pCategory->AddChildItem(new CXTPPropertyGridItemDouble(xml->str(1004), 0.0, _T("%.2f")));
	pItem1->SetID(IDC_DCLS);

	// H�jd �ver havet
	CXTPPropertyGridItemNumber *pItem2;
	pItem2 = (CXTPPropertyGridItemNumber*)pCategory->AddChildItem(new CXTPPropertyGridItemNumber(xml->str(1005)));
	pItem2->SetID(IDC_HOH);

	// Latitud
	pItem2 = (CXTPPropertyGridItemNumber*)pCategory->AddChildItem(new CXTPPropertyGridItemNumber(xml->str(1006)));
	pItem2->SetID(IDC_LATITUD);

	// H100
	CXTPPropertyGridItem *pSIH100 = (CXTPPropertyGridItem*)pCategory->AddChildItem(new CXTPPropertyGridItem(xml->str(1008)));
	pSIH100->SetMask(_T(">99"),_T("___"));
	pSIH100->SetID(IDC_H100);


	// Inst�llningar - s�derbergs bark- och h�jdfunktioner
	pCategory = m_propertyGrid.AddCategory(xml->str(220));

	// Kustn�ra
	CXTPPropertyGridItemBool *pItem3 = (CXTPPropertyGridItemBool*)pCategory->AddChildItem(new CMyPropGridItemBool(xml->str(204), xml->str(205), xml->str(2200), FALSE));
	pItem3->SetID(IDC_ATCOAST);

	// Syd�stra Sverige
	pItem3 = (CXTPPropertyGridItemBool*)pCategory->AddChildItem(new CMyPropGridItemBool(xml->str(204), xml->str(205), xml->str(2201), FALSE));
	pItem3->SetID(IDC_SOUTHEAST);

	// Region 5
	pItem3 = (CXTPPropertyGridItemBool*)pCategory->AddChildItem(new CMyPropGridItemBool(xml->str(204), xml->str(205), xml->str(2202), FALSE));
	pItem3->SetID(IDC_REGION5);

	// Delyta
	pItem3 = (CXTPPropertyGridItemBool*)pCategory->AddChildItem(new CMyPropGridItemBool(xml->str(204), xml->str(205), xml->str(2203), FALSE));
	pItem3->SetID(IDC_PART_OF_PLOT);

	// H100 Tall
	CXTPPropertyGridItem *pItem4 = (CXTPPropertyGridItem*)pCategory->AddChildItem(new CXTPPropertyGridItem(xml->str(2204)));
	pItem4->SetID(IDC_SIH100_PINE);
	pItem4->SetMask(_T("T00"), _T("T__"));

	if(xml) delete xml;


	// Give pointer to current template to specie views
	for( int i = 0; i < m_tabControl.GetItemCount(); i++ )
	{
		CSpecieSettingsFormView* pView = DYNAMIC_DOWNCAST(CSpecieSettingsFormView, CWnd::FromHandle(m_tabControl.GetItem(i)->GetHandle()));
		pView->SetCurTemplate(&m_template);
	}

	// Expand all categories
	_DoCollapseExpand( m_propertyGrid.GetCategories(), &CXTPPropertyGridItem::Expand );
}

int CTemplateFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CXTResizeFormView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Create tab control for specie settings
	m_tabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_TABCONTROL);
	m_tabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_tabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_tabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_tabControl.GetPaintManager()->DisableLunaColors(FALSE);
	m_tabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

	// Create property grid for generic settings
	m_propertyGrid.Create(CRect(0, 0, 0, 0), this, IDC_PROPERTYGRID);
	m_propertyGrid.SetOwner(this);
	m_propertyGrid.ShowHelp(FALSE);
	m_propertyGrid.SetViewDivider(0.5);
	m_propertyGrid.SetTheme(xtpGridThemeOffice2003);

	// Add specie tabs
	AddView(RUNTIME_CLASS(CSpecieSettingsFormView), getLangStr(STRID_TALL), 3);
	AddView(RUNTIME_CLASS(CSpecieSettingsFormView), getLangStr(STRID_GRAN), 3);
	AddView(RUNTIME_CLASS(CSpecieSettingsFormView), getLangStr(STRID_LOV), 3);
	AddView(RUNTIME_CLASS(CSpecieSettingsFormView), getLangStr(STRID_OVRLOV), 3);

	return 0;
}

LRESULT CTemplateFormView::OnMessageFromSuite(WPARAM wParam, LPARAM lParam)
{
	switch( wParam )
	{
	case ID_DBNAVIG_START:
	case ID_DBNAVIG_END:
	case ID_DBNAVIG_NEXT:
	case ID_DBNAVIG_PREV:
	case ID_NEW_ITEM:
		break;

	case ID_SAVE_ITEM:
		ExportToFile();
		break;

	case ID_DELETE_ITEM:
		break;
	}

	return 0L;
}

void CTemplateFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_group2.GetSafeHwnd())
	{
		setResize(&m_group2, 2, 20, cx-4, 100);
	}

	if (m_group1.GetSafeHwnd())
	{
		setResize(&m_group1, 2, 120, cx-4, cy-125);
	}

	if (m_propertyGrid.GetSafeHwnd())
	{
		m_propertyGrid.MoveWindow(10, 135, 290, cy-145);
	}

	if (m_tabControl.GetSafeHwnd())
	{
		m_tabControl.MoveWindow(320, 135, cx-330, cy-145);
	}
}

LRESULT CTemplateFormView::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	// Property grid value updated - reflect changes in template instance
	if( wParam == XTP_PGN_ITEMVALUE_CHANGED )
	{
		CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;

		switch( pItem->GetID() )
		{
		case IDC_DCLS:
			m_template.m_dcls = ((CXTPPropertyGridItemDouble*)pItem)->GetDouble();
			break;
		case IDC_HOH:
			m_template.m_hoh = ((CXTPPropertyGridItemNumber*)pItem)->GetNumber();
			break;
		case IDC_LATITUD:
			m_template.m_latitud = ((CXTPPropertyGridItemNumber*)pItem)->GetNumber();
			break;
		case IDC_H100:
			m_template.m_siH100 = pItem->GetValue();
			break;
		case IDC_ATCOAST:
			m_template.m_nearCoast = ((CXTPPropertyGridItemBool*)pItem)->GetBool();
			break;
		case IDC_SOUTHEAST:
			m_template.m_southEast = ((CXTPPropertyGridItemBool*)pItem)->GetBool();
			break;
		case IDC_REGION5:
			m_template.m_region5 = ((CXTPPropertyGridItemBool*)pItem)->GetBool();
			break;
		case IDC_PART_OF_PLOT:
			m_template.m_partOfPlot = ((CXTPPropertyGridItemBool*)pItem)->GetBool();
			break;
		case IDC_SIH100_PINE:
			m_template.m_h100Pine = pItem->GetValue();
			break;
		}
	}

	return 0L;
}

void CTemplateFormView::OnTextChanged(UINT nID)
{
	// Reflect changes in template instance
	switch( nID )
	{
	case IDC_EDIT1:
		m_template.m_name = m_name.getText();
		break;

	case IDC_EDIT2:
		m_template.m_createdby = m_createdby.getText();
		break;

	case IDC_EDIT3:
		m_template.m_date = m_date.getText();
		break;

	case IDC_EDIT4:
		m_template.m_notes = m_notes.getText();
		break;
	}
}

void CTemplateFormView::OnStatusChanged()
{
	m_template.m_status = m_status.GetCurSel();
}

BOOL CTemplateFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CSpecieSettingsFormView* pWnd;
	TRY
	{
		pWnd = (CSpecieSettingsFormView*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	int nTab = m_tabControl.GetItemCount();
	if( !pWnd->CreateWnd(dwStyle, rect, &m_tabControl, AFX_IDW_PANE_FIRST + nTab, &contextT) )
	{
		return FALSE;
	}
	m_tabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SetSpecieID(nTab);
	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CTemplateFormView::ExportToFile()
{
	CFileDialog dlg(FALSE, _T(".hgm"), _T(""), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("Haglof Gallringsmall (*.hgm)|*.hgm||"));
	if( dlg.DoModal() == IDOK )
	{
		m_template.SaveToFile(dlg.GetPathName());
	}
}
