#include "stdafx.h"
#include "UMThinningDB.h"
#include "UMThinningGenerics.h"

const TCHAR CUMThinningDB::SPCQUERY[4][3] = {{_T("=1")}, {_T("=2")}, {_T("=3")}, {_T(">3")}};

CUMThinningDB::CUMThinningDB(DB_CONNECTION_DATA &db_connection)
{
}

CUMThinningDB::~CUMThinningDB()
{
}

bool CUMThinningDB::GetThinningData(int traktId, TREEATTRIBUTE attribute, ResultDataVector &vec)
{
	SAString sql;
	RESULTDATA rd;

	try
	{
		// Loop for every set of species
		for( int i = 0; i < 4; i++ )
		{
			// Wipe out previous data
			memset( &rd, 0, sizeof(rd) );

			// Count trees by specie
			sql.Format( _T("SELECT SUM(tdata_gy) gy, SUM(tdata_numof) antal, SUM(tdata_dg) dg FROM esti_trakt_data_table WHERE tdata_trakt_id = :1 AND tdata_data_type = :2 AND tdata_spc_id %s"),
						CUMThinningDB::SPCQUERY[i] );
			m_saCommand.setCommandText( sql );
			m_saCommand.Param(1).setAsLong() = traktId;
			m_saCommand.Param(2).setAsLong() = attribute;
			m_saCommand.Execute();
			if( m_saCommand.FetchNext() )
			{
				rd.gyta		= m_saCommand.Field("gy").asDouble();
				rd.antal	= m_saCommand.Field("antal").asLong();
				rd.dg		= m_saCommand.Field("dg").asDouble();
			}

			vec.push_back(rd);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CUMThinningDB::GetThinningDataEx(int traktId, DWORD treetypes, ResultDataExVector &vec)
{
	SAString sql, dt;
	RESULTDATAEX rd;

	// tdata_data_type [1,2,3] represent "uttag", "kvarvarande", "uttag i stickv�g" respectively

	// Check which tree types to include in query
	if( treetypes & TA_REMOVAL )
	{
		if( !dt.IsEmpty() )
		{
			dt += _T(" OR ");
		}
		dt += _T("tdata_data_type = 1");
	}
	if( treetypes & TA_REMAINING )
	{
		if( !dt.IsEmpty() )
		{
			dt += _T(" OR ");
		}
		dt += _T("tdata_data_type = 2");
	}
	if( treetypes & TA_REMOVALINROAD )
	{
		if( !dt.IsEmpty() )
		{
			dt += _T(" OR ");
		}
		dt += _T("tdata_data_type = 3");
	}

	if( dt.IsEmpty() )
	{
		// Unknown tree type
		return false;
	}

	try
	{
		// Loop for every set of species
		for( int i = 0; i < 4; i++ )
		{
			// Wipe out previous data
			memset( &rd, 0, sizeof(rd) );

			// Get data per specie for this stand
			sql.Format( _T("SELECT SUM(tdata_m3sk_vol) m3sk, SUM(tdata_m3fub_vol) m3fub, SUM(tdata_gy) gy, SUM(tdata_dg) dg, SUM(tdata_numof) antal, SUM(tdata_hgv) hgv, SUM(tdata_h25) h25 ")
						_T("FROM esti_trakt_data_table WHERE tdata_trakt_id = :1 AND (%s) AND tdata_spc_id %s"),
						dt, CUMThinningDB::SPCQUERY[i] );
			m_saCommand.setCommandText( sql );
			m_saCommand.Param(1).setAsLong() = traktId;
			m_saCommand.Execute();
			if( m_saCommand.FetchNext() )
			{
				rd.m3sk		= m_saCommand.Field("m3sk").asDouble();
				rd.m3fub	= m_saCommand.Field("m3fub").asDouble();
				rd.gyta		= m_saCommand.Field("gy").asDouble();
				rd.dg		= m_saCommand.Field("dg").asDouble();
				rd.antal	= m_saCommand.Field("antal").asLong();
				rd.hgv		= m_saCommand.Field("hgv").asDouble();
				rd.h25		= m_saCommand.Field("h25").asDouble();
				if( rd.antal > 0 )
				{
					rd.m3skAvg  = rd.m3sk / rd.antal;
					rd.m3fubAvg = rd.m3fub / rd.antal;
				}

				// TODO: ga.kvot
			}

			vec.push_back(rd);
		};
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CUMThinningDB::GetAvgPlotData(int traktId, AVGPLOTDATA &data)
{
	try
	{
	m_saCommand.setCommandText (_T("SELECT skTrad, skStamm, skRot, skRota, stvAvst, stvBredd, (100 * stvBredd / stvAvst) stvAreal, sparLangd, sparDjup, (100 * sparDjup / sparLangd) sparDjupProc ")
									_T("FROM (SELECT ")
									_T("(100 * SUM(skadorStammZon1 + skadorStammZon2 + skadorRotZon1 + skadorRotZon2) / SUM(antalStamm + antalStubb)) skTrad, ")
									_T("(100 * SUM(skadorStammZon1 + skadorStammZon2) / SUM(antalStamm + antalStubb)) skStamm, ")
									_T("(100 * SUM(skadorRotZon1 + skadorRotZon2) / SUM(antalStamm + antalStubb)) skRot, ")
									_T("(100 * SUM(skadorRota) / SUM(antalStamm + antalStubb)) skRota, ")
									_T("AVG((stvavst1 + stvavst2) / 2) stvAvst, ")
									_T("AVG((stvBredd1 + stvBredd2) / 2) stvBredd, ")
									_T("AVG(sparLangd) sparLangd, ")
									_T("AVG((sparDjup1 + sparDjup2) / 2) sparDjup ")
									_T("FROM thn_plot WHERE traktId = :1) dt") );
		m_saCommand.Param(1).setAsLong() = traktId;
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			data.skTrad			= m_saCommand.Field("skTrad").asLong();
			data.skStamm		= m_saCommand.Field("skStamm").asLong();
			data.skRot			= m_saCommand.Field("skRot").asLong();
			data.skRota			= m_saCommand.Field("skRota").asLong();
			data.stvAvstand		= m_saCommand.Field("stvAvst").asLong();
			data.stvBredd		= m_saCommand.Field("stvBredd").asLong();
			data.stvAreal		= m_saCommand.Field("stvAreal").asLong();
			data.sparStracka	= m_saCommand.Field("sparLangd").asLong();
			data.sparDjup		= m_saCommand.Field("sparDjup").asLong();
			data.sparDjupProc	= m_saCommand.Field("sparDjupProc").asLong();
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CUMThinningDB::GetPlotData(int traktId, PlotDataVector &vec)
{
	PLOTDATA pd;

	try
	{ 
		m_saCommand.setCommandText( _T("SELECT * FROM thn_plot WHERE traktId = :1") );
		m_saCommand.Param(1).setAsLong() = traktId;
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			// Wipe out previous data
			memset( &pd, 0, sizeof(pd) );

			pd.gyStamm		= m_saCommand.Field("gyStamm").asLong();
			pd.gyStubb		= m_saCommand.Field("gyStubb").asLong();
			pd.antalStamm	= m_saCommand.Field("antalStamm").asLong();
			pd.antalStubb	= m_saCommand.Field("antalStubb").asLong();
			pd.skStamm		= m_saCommand.Field("skadorStammZon1").asLong() + m_saCommand.Field("skadorStammZon2").asLong();
			pd.skRot		= m_saCommand.Field("skadorRotZon1").asLong() + m_saCommand.Field("skadorRotZon2").asLong();
			pd.skStRt		= m_saCommand.Field("skadorStRtZon1").asLong() + m_saCommand.Field("skadorStRtZon2").asLong();
			pd.skRota		= m_saCommand.Field("skadorRota").asLong();
			vec.push_back(pd);
		};
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}
