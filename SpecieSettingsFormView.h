#pragma once

#include "Resource.h"

class CThinningTemplate;

class CSpecieSettingsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSpecieSettingsFormView)

protected:
	CSpecieSettingsFormView();           // protected constructor used by dynamic creation
	virtual ~CSpecieSettingsFormView();

	void AddFunctions(LPCTSTR lpszName, vecUCFunctions &vecFunc, CXTPPropertyGridItem *pParent, int nID);
	void SetupSubFunctions(CXTPPropertyGridItem *pItem, CString caption, vecUCFunctions &vecFunc, vecUCFunctionList &vecFuncList, int nID);

public:
	enum { IDD = IDD_FORMVIEW15 };

	BOOL CreateWnd(DWORD dwStyle, const RECT &rect, CWnd *pParent, UINT nId, CCreateContext *pCC);
	void SetSpecieID(int nID) { m_nSpcID = nID; }
	void SetCurTemplate(CThinningTemplate *pTemplate) { m_pTemplate = pTemplate; }

	virtual void OnInitialUpdate();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	bool m_bInitialized;
	CThinningTemplate *m_pTemplate;
	CMyPropertyGrid m_propertyGrid;
	CString m_sCalcAsHeight, m_sCalcAsVolume, m_sCalcAsBark, m_sCalcAsVolumeUB;
	int m_nSpcID;

	afx_msg LRESULT OnGridNotify(WPARAM wParam, LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	DECLARE_MESSAGE_MAP()
};
