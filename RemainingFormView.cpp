// PageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "UMThinningDB.h"
#include "UMThinningGenerics.h"
#include "MDITabbedView.h"
#include "StandEntryDoc.h"

#include "RemainingFormView.h"
#include "ResLangFileReader.h"

// CRemainingFormView

IMPLEMENT_DYNCREATE(CRemainingFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CRemainingFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
END_MESSAGE_MAP()

CRemainingFormView::CRemainingFormView():
CXTResizeFormView(CRemainingFormView::IDD),
m_pDB(NULL)
{
}

CRemainingFormView::~CRemainingFormView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}

void CRemainingFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GROUP1, m_group1);
	DDX_Control(pDX, IDC_EDIT1, m_avlagg);
	DDX_Control(pDX, IDC_EDIT2, m_drvenhet);
	DDX_Control(pDX, IDC_EDIT3, m_datum);
	DDX_Control(pDX, IDC_EDIT4, m_koordinat);
}

void CRemainingFormView::PopulateData()
{
	int idx;
	CString str;
	CTransaction_trakt *pTrakt = getActiveTrakt();
	ResultDataExVector rex;

	m_report.ResetContent(FALSE);

	// List general data
	m_avlagg.SetWindowText(pTrakt->getTraktName());
	m_drvenhet.SetWindowText(pTrakt->getTraktPNum());
	m_datum.SetWindowText(pTrakt->getTraktDate());
	str.Format(_T("%d, %d"), pTrakt->getTraktLatitude(), pTrakt->getTraktLongitude());
	m_koordinat.SetWindowText(str);

	// List data for this stand
	idx = 0;
	m_pDB->GetThinningDataEx(pTrakt->getTraktID(), TA_REMAINING, rex);
	ResultDataExVector::const_iterator iter = rex.begin();
	while( iter != rex.end() )
	{
		CXTPReportRecord *pRec = new CXTPReportRecord;
		pRec->AddItem(new CXTPReportRecordItemText(g_specie[idx++]));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3sk));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3skAvg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3fub));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->m3fubAvg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->gyta));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->dg));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->antal));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->hgv));
		pRec->AddItem(new CXTPReportRecordItemNumber(iter->h25));
		m_report.AddRecord(pRec);

		iter++;
	}

	m_report.Populate();
	m_report.UpdateWindow();
}

void CRemainingFormView::OnInitialUpdate()
{
	CXTPReportColumn *pCol;

	SetDlgItemText(IDC_STATIC1, getLangStr(STRID_AVLAGG));
	SetDlgItemText(IDC_STATIC2, getLangStr(STRID_DRVENHET));
	SetDlgItemText(IDC_STATIC3, getLangStr(STRID_DATUM));
	SetDlgItemText(IDC_STATIC4, getLangStr(STRID_KOORDINAT));

	m_avlagg.SetDisabledColor(BLACK, INFOBK);
	m_avlagg.SetReadOnly();

	m_drvenhet.SetDisabledColor(BLACK, INFOBK);
	m_drvenhet.SetReadOnly();

	m_datum.SetDisabledColor(BLACK, INFOBK);
	m_datum.SetReadOnly();

	m_koordinat.SetDisabledColor(BLACK, INFOBK);
	m_koordinat.SetReadOnly();


	// Set up report
	if( m_report.Create(this, IDC_REPORTPLOTS, TRUE, FALSE) )
	{
		pCol = m_report.AddColumn(new CXTPReportColumn(0, _T(""), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		pCol = m_report.AddColumn(new CXTPReportColumn(1, getLangStr(STRID_KVARVCOL1), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(2, getLangStr(STRID_KVARVCOL2), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(3, getLangStr(STRID_KVARVCOL3), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(4, getLangStr(STRID_KVARVCOL4), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(5, getLangStr(STRID_KVARVCOL5), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(6, getLangStr(STRID_KVARVCOL6), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(7,getLangStr(STRID_KVARVCOL7), 80));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(8, getLangStr(STRID_KVARVCOL8), 70));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);

		pCol = m_report.AddColumn(new CXTPReportColumn(9, getLangStr(STRID_KVARVCOL9), 70));
		pCol->AllowRemove(FALSE);
		pCol->SetHeaderAlignment(DT_WORDBREAK);
	}

	m_report.ModifyStyle(0, WS_BORDER);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());
}

BOOL CRemainingFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		m_pDB = new CUMThinningDB(*((DB_CONNECTION_DATA*)pData->lpData));
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CRemainingFormView::OnSize(UINT nType, int cx, int cy)
{
	RECT rect;

	GetClientRect(&rect);

	if( m_report.GetSafeHwnd() )
	{
		setResize( &m_report, 10, 100, 790, 110 );
	}
}
