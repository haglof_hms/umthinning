#pragma once

#include "Resource.h"

class CRemainingFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CRemainingFormView)

protected:
	CRemainingFormView();           // protected constructor used by dynamic creation
	virtual ~CRemainingFormView();

	CUMThinningDB *m_pDB;
	CMyReportCtrl m_report;

public:
	enum { IDD = IDD_FORMVIEW12 };

	void PopulateData();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();

	CXTResizeGroupBox m_group1;
	CMyExtEdit m_avlagg;
	CMyExtEdit m_drvenhet;
	CMyExtEdit m_datum;
	CMyExtEdit m_koordinat;

	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};
