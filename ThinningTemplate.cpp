#include "stdafx.h"
#include "ThinningTemplate.h"
#include "UMThinningGenerics.h"

CThinningTemplate::CThinningTemplate():
m_nearCoast(FALSE),
m_southEast(FALSE),
m_region5(FALSE),
m_partOfPlot(FALSE),
m_dcls(0),
m_status(1),
m_hoh(0),
m_latitud(0)
{
	memset(m_spc, -1, sizeof(m_spc));
	for( int i = 0; i < 4; i++ )
	{
		m_spc[i].h25 = 0;
		m_spc[i].gcrown = 0;
	}
}

CThinningTemplate::~CThinningTemplate()
{
}

bool CThinningTemplate::LoadFromBuffer(CString &buffer)
{
	// TODO: implement
	return true;
}

bool CThinningTemplate::LoadFromFile(CString &filename)
{
	// TODO: implement
	return true;
}

bool CThinningTemplate::SaveToFile(CString &filename)
{
	CFile file;
	CString str;

	if( !file.Open(filename, CFile::modeCreate | CFile::modeWrite) )
	{
		return false;
	}

	// Write Byte Order Mark (we use UTF16 to eliminate conversations between wide char and multibyte)
	file.Write( UTF16_BOM, sizeof(UTF16_BOM) );

	str.Format( _T("<?xml version=\"1.0\" encoding=\"UTF-16\" ?>") );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<ThinningTemplate>") );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<Name>%s</Name>"), m_name );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<CreatedBy>%s</CreatedBy>"), m_createdby );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<Date>%s</Date>"), m_date );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<Dcls>%.2f</Dcls>"), m_dcls );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<HOH>%d</HOH>"), m_hoh );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<Latitude>%d</Latitude>"), m_latitud );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<H100>%s</H100>"), m_siH100 );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<NearCoast>%d</NearCoast>"), m_nearCoast );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<SouthEast>%d</SouthEast>"), m_southEast );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<Region5>%d</Region5>"), m_region5 );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<PartOfPlot>%d</PartOfPlot>"), m_partOfPlot );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );
	str.Format( _T("<H100Pine>%s</H100Pine>"), m_h100Pine );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );

	for( int i = 0; i < 4; i++ )
	{
		str.Format( _T("<Specie idx=\"%d\">"), i );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
		str.Format( _T("<FuncHeight idx=\"%d\" sub=\"%d\"/>"), m_spc[i].funcHeight, m_spc[i].subHeight );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
		str.Format( _T("<FuncVolume idx=\"%d\" sub=\"%d\"/>"), m_spc[i].funcVolume, m_spc[i].subVolume );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
		str.Format( _T("<FuncBark idx=\"%d\" sub=\"%d\"/>"), m_spc[i].funcBark, m_spc[i].subBark );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
		str.Format( _T("<FuncVolumeUB idx=\"%d\" sub=\"%d\"/>"), m_spc[i].funcVolumeUB, m_spc[i].subVolumeUB );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
		str.Format( _T("<H25>%.1f</H25>"), m_spc[i].h25 );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
		str.Format( _T("<GCrown>%d</GCrown>"), m_spc[i].gcrown );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
		str.Format( _T("</Specie>") );
		file.Write( str, str.GetLength() * sizeof(TCHAR) );
	}

	str.Format( _T("</ThinningTemplate>") );
	file.Write( str, str.GetLength() * sizeof(TCHAR) );

	file.Close();

	return true;
}
