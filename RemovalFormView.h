#pragma once

#include "Resource.h"

class CUMThinningDB;

class CRemovalFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CRemovalFormView)

protected:
	CRemovalFormView();           // protected constructor used by dynamic creation
	virtual ~CRemovalFormView();

	CUMThinningDB *m_pDB;
	CMyReportCtrl m_report1;
	CMyReportCtrl m_report2;
	CMyReportCtrl m_report3;

public:
	enum { IDD = IDD_FORMVIEW13 };

	void PopulateData();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();

	CXTResizeGroupBox m_group1;
	CMyExtEdit m_avlagg;
	CMyExtEdit m_drvenhet;
	CMyExtEdit m_datum;
	CMyExtEdit m_koordinat;

	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};
