#include "UMThinningGenerics.h"
#include "ResLangFileReader.h"


CString getLangStr(DWORD resid)
{
	CString langFile, string;

	// Obtain specified string from language file
	langFile.Format( _T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAMETHINNING, getLangSet(), LANGUAGE_FN_EXT );
	if( fileExists( langFile ) )
	{
		RLFReader *xml = new RLFReader;
		if( xml->Load( langFile ) )
		{
			string = xml->str( resid );
			string.Replace(_T("\\n"), _T("\n")); // Convert \n in string to line-breaks
		}
		delete xml;
	}

	return string;
}

void _DoCollapseExpand(CXTPPropertyGridItems* pItems, ITEMFUNCTIONPTR pFunction)
{
	for (int i = 0; i < pItems->GetCount(); i++)
	{
		CXTPPropertyGridItem* pItem = pItems->GetAt(i);
		if (pItem->HasChilds())
		{
			(pItem->*pFunction)();
			_DoCollapseExpand(pItem->GetChilds(), pFunction);
		}
	}
}
