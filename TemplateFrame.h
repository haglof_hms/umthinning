#pragma once

#include "Resource.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CTemplateFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CTemplateFrame)

public:
	CTemplateFrame();
	virtual ~CTemplateFrame();

protected:
	BOOL m_bFirstShow;

	afx_msg LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	DECLARE_MESSAGE_MAP()
};
